import numpy as np
from scipy.special.orthogonal import p_roots

#first, we will do piecewise linear integration
print "First, we will use piecewise integration to integrate f(x)= x^3 + 2x^2 - 4"

def linear(x1,x2):
	def f(x):
		return (x ** 3) + (2*(x ** 2)) - 4
		
	h = (x2 - x1) / 1000.
	#for 1000 subdivisions
	
	list = []
	for i in range(0,1000):
		list.append(h * (f(i*h + x1)))
		
	sum = 0
	for n in list:
		sum = sum + n
	return sum

print linear(-1,1)

print "The actual value is -6.666666 repeating. Our calculated value for 1000 subdivisions is -6.668664."
print "Therefore, the absolute error is only 0.001998."

#now, we will do trapezoidal integration
print "Now, we will use the trapezoidal rule."

def f(x):
	return (x ** 3) + (2*(x ** 2)) - 4

def trapezoidal(f, a, b, n):
    h = float(b - a) / n
    s = 0.0
    s += f(a)/2.0
    for i in range(1, n):
        s += f(a + i*h)
    s += f(b)/2.0
    return s * h
    
print trapezoidal(f, -1, 1, 1000)

print "The actual value is -6.666666 repeating. Our calculated value for 1000 subdivisions is -6.666664."
print "Therefore, the absolute error is only 0.000002."

#now, we will use Simpsons Rule
print "Now, we will use Simpson's Rule"

def simpson(f, a, b, n):
    h=float(b-a)/n
    k=0.0
    x=a + h
    for i in range(1,n/2 + 1):
        k += 4*f(x)
        x += 2*h

    x = a + 2*h
    for i in range(1,n/2):
        k += 2*f(x)
        x += 2*h
    return (h/3)*(f(a)+f(b)+k)

print simpson(f,-1,1,1000)

print "The actual value is -6.666666 repeating. Our calculated value for 1000 subdivisions is -6.666666666667."
print "Therefore, the absolute error is only 0.000000000001."

#now, we will integrate using Gaussian Quadrature
print "Now, we will integrate using Gaussian Quadrature."


def gaussian(f,n,a,b):
    [x,w] = p_roots(n+1)
    G=0.5*(b-a)*sum(w*f(0.5*(b-a)*x+0.5*(b+a)))
    return G

print gaussian(f,2,-1,1)

print "The actual value is -6.666666 repeating. Our calculated value for 1000 subdivisions is -6.666666666667."
print "Therefore, the absolute error is only 0.000000000001."
print "Both Simpson's rule and Gaussian Quadrature maxed out Python's precision."


