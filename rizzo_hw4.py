import numpy as np
import math
import matplotlib.pyplot as plt

def r(z):
		return (3000/(math.sqrt((0.3 * ((1 + z) ** 3)+ 0.7))))

def trapezoidal(f, a, b, n):
    h = float(b - a) / n
    s = 0.0
    s += f(a)/2.0
    for i in range(1, n):
        s += f(a + i*h)
    s += f(b)/2.0
    return s * h
	

def da(f,a,b,n):
	return trapezoidal(f,a,b,n) / (1+b)
	


da_points = []

x_points = []

for i in range(0,1000):
	x_points.append((5./1000)*i)

for i in x_points:
	da_points.append(da(r,0,i,1000))
	
#now I will use the numpy.trapz function and plot it against my function

ypointz0 = []
ypointz1 = []
ypointz2 = []
ypointz3 = []
ypointz4 = []
ypointz5 = []

x_pointz0 = []
x_pointz1 = []
x_pointz2 = []
x_pointz3 = []
x_pointz4 = []
x_pointz5 = []

for i in range(0,1000):
	x_pointz0.append(0)
	
for i in range(0,1000):
	x_pointz1.append((1./1000)*i)

for i in range(0,1000):
	x_pointz2.append((2./1000)*i)
	
for i in range(0,1000):
	x_pointz3.append((3./1000)*i)
	
for i in range(0,1000):
	x_pointz4.append((4./1000)*i)

for i in range(0,1000):
	x_pointz5.append((5./1000)*i)


for i in range(0,1000):
	ypointz0.append(r(0)/1000)

for i in range(0,1000):
	ypointz1.append(r((1.*i)/1000))
		
for i in range(0,1000):
	ypointz2.append(r((2.*i)/1000))
	
for i in range(0,1000):
	ypointz3.append(r((3.*i)/1000))
	
for i in range(0,1000):
	ypointz4.append(r((4.*i)/1000))
	
for i in range(0,1000):
	ypointz5.append(r((5.*i)/1000))
	

trapda0 = np.trapz(ypointz0,x=x_pointz0) / 1
trapda1 = np.trapz(ypointz1,x=x_pointz1) / 2
trapda2 = np.trapz(ypointz2,x=x_pointz2) / 3
trapda3 = np.trapz(ypointz3,x=x_pointz3) / 4
trapda4 = np.trapz(ypointz4,x=x_pointz4) / 5
trapda5 = np.trapz(ypointz5,x=x_pointz5) / 6


trapda = [trapda0,trapda1,trapda2,trapda3,trapda4,trapda5]
trapdax = [0,1,2,3,4,5]
	
x = range(0,5)
y = range(0,1300)
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(trapdax, trapda, s=150, c='r', marker="o", label='nptrapz')
ax1.scatter(x_points, da_points, s=30, c='b', marker="s", label='created')

plt.legend(loc='upper left');
plt.show()

#the np.trapz points were made very large to emphasize the fit. The data from np.trapz fits my trapezoidal integrator almost perfectly.

	