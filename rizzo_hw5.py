import numpy as np
import matplotlib.pyplot as plt

def bisection(function,left_bound,right_bound,tolerance=1.0e-4):
	fr = function(right_bound)
	if fr == 0.0: 
		return right_bound
	fl = function(left_bound)
	if fl == 0.0: 
		return left_bound
	if fr*fl > 0.0:
		return None
	else:
		mid = (left_bound + right_bound) / 2.0
		error = (right_bound - left_bound) / 2.0
		while (error > tolerance):
			fl = function(left_bound)
			fr = function(right_bound)
			fm = function(mid)
			
			if fm==0.0:
				return mid
			
			elif fl*fm < 0.0:
				right_bound = mid
				
			elif fr*fm < 0.0:
				left_bound = mid
				
			mid = (left_bound + right_bound) / 2.0
			error = (right_bound - left_bound) / 2.0
		return mid
		
def f(x):
	return np.sin(x)
	
def g(x):
	return (x**3.0)-x-2.0
	
def y(x):
	return (-6.0)+x+(x**2.0)
	
	
a = bisection(f,2.0,4.0)
b = bisection(g,1.0,2.0)
c = bisection(y,0.0,5.0)

print a
print "This is the value of the root of f(x)"
print b
print "This is the value of the root of g(x)"
print c
print "This is the value of the root of y(x)"

X = np.linspace(-np.pi, np.pi, 256, endpoint=True)


plt.plot(X,(X**3.0)-X-2.0, label = 'gx')
plt.plot(X,(-6.0)+X+(X**2.0), label = 'yx')
plt.plot(X, np.sin(X), label = 'fx')
plt.plot(X, X*0.0, label = 'x_axis')
plt.ylim((-10,10))

plt.title("Functions")

plt.legend()

plt.show()


