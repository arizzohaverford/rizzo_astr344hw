import math
import numpy as np
import random
import copy
import pdb

def randexp():
	return -math.log(1.0-random.random())


def move(j,m,num):
	for i in range(0,num):
		if j[i] < 0: #so they don't move beyond the boundaries of the first quadrant
			j[i] = 1
		else: #if not on boundary, move randomly
			j[i] = j[i] + random.randrange(-5,5,1)
		
	for k in range(0,num):
		if m[k] < 0: #so they don't move beyond the boundaries of the first quadrant
			m[k] = 1
		else: #if not on boundary, move randomly
			m[k] = m[k] + random.randrange(-5,5,1)
		
	

xpos = []
ypos = []


for i in range(0,100): #making my coordinate lists
	xpos.append(random.randrange(100))
	
for i in range(0,100):
	ypos.append(random.randrange(100))
	
infected = []

for i in range(0,100):
	infected.append(False)
	
x = np.asarray(xpos)
y = np.asarray(ypos)


	
#initialize the infection	
infected[0] = True

counter = 0

while all(infected) == False:
	move(x,y,100) #move people every time step
	for i in range(0,100):
		if infected[i] == True: #check which people are infected
			distance = np.sqrt( (x-x[i])**2. + (y-y[i])**2.)
			for k in range(0,100):
				if distance[k] < 5.:
					prob = random.random()
					if prob < .75:
						if infected[k] == False:
							infected[k] = True
							 
	counter += 1
	
print counter

xpos_one = []
ypos_one = []

for i in range(0,90): #making my coordinate lists
	xpos_one.append(random.randrange(100))
	
for i in range(0,90):
	ypos_one.append(random.randrange(100))
	
infected_one = []

for i in range(0,90):
	infected_one.append(False)
	
x_one = np.asarray(xpos_one)
y_one = np.asarray(ypos_one)

	
#initialize the infection	
infected_one[0] = True

counter_one = 0

while all(infected_one) == False:
	move(x_one,y_one,90) #move people every time step
	for i in range(0,90):
		if infected_one[i] == True: #check which people are infected
			distance_one = np.sqrt( (x_one-x_one[i])**2. + (y_one-y_one[i])**2.)
			for k in range(0,90):
				if distance_one[k] < 5.:
					prob_one = random.random()
					if prob_one < .75:
						if infected_one[k] == False:
							infected_one[k] = True
							 
	counter_one += 1
	


xpos_two = []
ypos_two = []

for i in range(0,80): #making my coordinate lists
	xpos_two.append(random.randrange(100))
	
for i in range(0,80):
	ypos_two.append(random.randrange(100))
	
infected_two = []

for i in range(0,80):
	infected_two.append(False)
	
x_two = np.asarray(xpos_two)
y_two = np.asarray(ypos_two)

	
#initialize the infection	
infected_two[0] = True

counter_two = 0

while all(infected_two) == False:
	move(x_two,y_two,80) #move people every time step
	for i in range(0,80):
		if infected_two[i] == True: #check which people are infected
			distance_two = np.sqrt( (x_two-x_two[i])**2. + (y_two-y_two[i])**2.)
			for k in range(0,80):
				if distance_two[k] < 5.:
					prob_two = random.random()
					if prob_two < .75:
						if infected_two[k] == False:
							infected_two[k] = True
							 
	counter_two += 1


xpos_three = []
ypos_three = []

for i in range(0,70): #making my coordinate lists
	xpos_three.append(random.randrange(100))
	
for i in range(0,70):
	ypos_three.append(random.randrange(100))
	
infected_three = []

for i in range(0,70):
	infected_three.append(False)
	
x_three = np.asarray(xpos_three)
y_three = np.asarray(ypos_three)

	
#initialize the infection	
infected_three[0] = True

counter_three = 0

while all(infected_three) == False:
	move(x_three,y_three,70) #move people every time step
	for i in range(0,70):
		if infected_three[i] == True: #check which people are infected
			distance_three = np.sqrt( (x_three-x_three[i])**2. + (y_three-y_three[i])**2.)
			for k in range(0,70):
				if distance_three[k] < 5.:
					prob_three = random.random()
					if prob_three < .75:
						if infected_three[k] == False:
							infected_three[k] = True
							 
	counter_three += 1
	


xpos_four = []
ypos_four = []

for i in range(0,60): #making my coordinate lists
	xpos_four.append(random.randrange(100))
	
for i in range(0,60):
	ypos_four.append(random.randrange(100))
	
infected_four = []

for i in range(0,60):
	infected_four.append(False)
	
x_four = np.asarray(xpos_four)
y_four = np.asarray(ypos_four)

	
#initialize the infection	
infected_four[0] = True

counter_four = 0

while all(infected_four) == False:
	move(x_four,y_four,60) #move people every time step
	for i in range(0,60):
		if infected_four[i] == True: #check which people are infected
			distance_four = np.sqrt( (x_four-x_four[i])**2. + (y_four-y_four[i])**2.)
			for k in range(0,60):
				if distance_four[k] < 5.:
					prob_four = random.random()
					if prob_four < .75:
						if infected_four[k] == False:
							infected_four[k] = True
							 
	counter_four += 1
	


xpos_five = []
ypos_five = []

for i in range(0,50): #making my coordinate lists
	xpos_five.append(random.randrange(100))
	
for i in range(0,50):
	ypos_five.append(random.randrange(100))
	
infected_five = []

for i in range(0,50):
	infected_five.append(False)
	
x_five = np.asarray(xpos_five)
y_five = np.asarray(ypos_five)

	
#initialize the infection	
infected_five[0] = True

counter_five = 0

while all(infected_five) == False:
	move(x_five,y_five,50) #move people every time step
	for i in range(0,50):
		if infected_five[i] == True: #check which people are infected
			distance_five = np.sqrt( (x_five-x_five[i])**2. + (y_five-y_five[i])**2.)
			for k in range(0,50):
				if distance_five[k] < 5.:
					prob_five = random.random()
					if prob_five < .75:
						if infected_five[k] == False:
							infected_five[k] = True
							 
	counter_five += 1
	


xpos_six = []
ypos_six = []

for i in range(0,40): #making my coordinate lists
	xpos_six.append(random.randrange(100))
	
for i in range(0,40):
	ypos_six.append(random.randrange(100))
	
infected_six = []

for i in range(0,40):
	infected_six.append(False)
	
x_six = np.asarray(xpos_six)
y_six = np.asarray(ypos_six)

	
#initialize the infection	
infected_six[0] = True

counter_six = 0

while all(infected_six) == False:
	move(x_six,y_six,40) #move people every time step
	for i in range(0,40):
		if infected_six[i] == True: #check which people are infected
			distance_six = np.sqrt( (x_six-x_six[i])**2. + (y_six-y_six[i])**2.)
			for k in range(0,40):
				if distance_six[k] < 5.:
					prob_six = random.random()
					if prob_six < .75:
						if infected_six[k] == False:
							infected_six[k] = True
							 
	counter_six += 1
	

xpos_sev = []
ypos_sev = []

for i in range(0,30): #making my coordinate lists
	xpos_sev.append(random.randrange(100))
	
for i in range(0,30):
	ypos_sev.append(random.randrange(100))
	
infected_sev = []

for i in range(0,30):
	infected_sev.append(False)
	
x_sev = np.asarray(xpos_sev)
y_sev = np.asarray(ypos_sev)

	
#initialize the infection	
infected_sev[0] = True

counter_sev = 0

while all(infected_sev) == False:
	move(x_sev,y_sev,30) #move people every time step
	for i in range(0,30):
		if infected_sev[i] == True: #check which people are infected
			distance_sev = np.sqrt( (x_sev-x_sev[i])**2. + (y_sev-y_sev[i])**2.)
			for k in range(0,30):
				if distance_sev[k] < 5.:
					prob_sev = random.random()
					if prob_sev < .75:
						if infected_sev[k] == False:
							infected_sev[k] = True
							 
	counter_sev += 1


xpos_ei = []
ypos_ei = []

for i in range(0,20): #making my coordinate lists
	xpos_ei.append(random.randrange(100))
	
for i in range(0,20):
	ypos_ei.append(random.randrange(100))
	
infected_ei = []

for i in range(0,20):
	infected_ei.append(False)
	
x_ei = np.asarray(xpos_ei)
y_ei = np.asarray(ypos_ei)

	
#initialize the infection	
infected_ei[0] = True

counter_ei = 0

while all(infected_ei) == False:
	move(x_ei,y_ei,20) #move people every time step
	for i in range(0,20):
		if infected_ei[i] == True: #check which people are infected
			distance_ei = np.sqrt( (x_ei-x_ei[i])**2. + (y_ei-y_ei[i])**2.)
			for k in range(0,20):
				if distance_ei[k] < 5.:
					prob_ei = random.random()
					if prob_ei < .75:
						if infected_ei[k] == False:
							infected_ei[k] = True
							 
	counter_ei += 1
	

xpos_nine = []
ypos_nine = []

for i in range(0,10): #making my coordinate lists
	xpos_nine.append(random.randrange(100))
	
for i in range(0,10):
	ypos_nine.append(random.randrange(100))
	
infected_nine = []

for i in range(0,10):
	infected_nine.append(False)
	
x_nine = np.asarray(xpos_nine)
y_nine = np.asarray(ypos_nine)

	
#initialize the infection	
infected_nine[0] = True

counter_nine = 0

while all(infected_nine) == False:
	move(x_nine,y_nine,10) #move people every time step
	for i in range(0,10):
		if infected_nine[i] == True: #check which people are infected
			distance_nine = np.sqrt( (x_nine-x_nine[i])**2. + (y_nine-y_nine[i])**2.)
			for k in range(0,10):
				if distance_nine[k] < 5.:
					prob_nine = random.random()
					if prob_nine < .75:
						if infected_nine[k] == False:
							infected_nine[k] = True
							 
	counter_nine += 1
	
xpos_nf = []
ypos_nf = []

for i in range(0,5): #making my coordinate lists
	xpos_nf.append(random.randrange(100))
	
for i in range(0,5):
	ypos_nf.append(random.randrange(100))
	
infected_nf = []

for i in range(0,5):
	infected_nf.append(False)
	
x_nf = np.asarray(xpos_nf)
y_nf = np.asarray(ypos_nf)

	
#initialize the infection	
infected_nf[0] = True

counter_nf = 0

while all(infected_nf) == False:
	move(x_nf,y_nf,5) #move people every time step
	for i in range(0,5):
		if infected_nf[i] == True: #check which people are infected
			distance_nf = np.sqrt( (x_nf-x_nf[i])**2. + (y_nf-y_nf[i])**2.)
			for k in range(0,5):
				if distance_nf[k] < 5.:
					prob_nf = random.random()
					if prob_nf < .75:
						if infected_nf[k] == False:
							infected_nf[k] = True
							 
	counter_nf += 1
	

#now, for when people don't like to get close


xpos_n = []
ypos_n = []

for i in range(0,100): #making my coordinate lists
	xpos_n.append(random.randrange(100))
	
for i in range(0,100):
	ypos_n.append(random.randrange(100))
	
infected_n = []

for i in range(0,100):
	infected_n.append(False)
	
x_n = np.asarray(xpos_n)
y_n = np.asarray(ypos_n)

	
#initialize the infection	
infected_n[0] = True

counter_n = 0

while all(infected_n) == False:
	move(x_n,y_n,100) #move people every time step
	for i in range(0,100):
		if infected_n[i] == True: #check which people are infected
			distance_n = np.sqrt( (x_n-x_n[i])**2. + (y_n-y_n[i])**2.)
			for k in range(0,100):
				if distance_n[k] < 5.:
					prob_n = random.random()
					if prob_n < .25:
						if infected_n[k] == False:
							infected_n[k] = True
							 
	counter_n += 1
	
print counter_n





