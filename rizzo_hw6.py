import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta
import math 

#part one

		
def f(x):
	return np.sin(x)
	
def g(x):
	return (x**3.0)-x-2.0
	
def y(x):
	return (-6.0)+x+(x**2.0)
	

def bisection(function,left_bound,right_bound,tolerance=1.0e-4):
	fr = function(right_bound)
	if fr == 0.0: 
		return right_bound
	fl = function(left_bound)
	if fl == 0.0: 
		return left_bound
	if fr*fl > 0.0:
		return None
	else:
		mid = (left_bound + right_bound) / 2.0
		error = (right_bound - left_bound) / 2.0
		while (error > tolerance):
			fl = function(left_bound)
			fr = function(right_bound)
			fm = function(mid)
			
			if fm==0.0:
				return mid
			
			elif fl*fm < 0.0:
				right_bound = mid
				
			elif fr*fm < 0.0:
				left_bound = mid
				
			mid = (left_bound + right_bound) / 2.0
			error = (right_bound - left_bound) / 2.0
		return mid

t1a = datetime.now()

print "This is the root of f(x) using the bisection method: ", bisection(f,2.0,4.0)

t2a = datetime.now()

print "This is the time for the bisection method for f(x) in microseconds: ", t2a.microsecond - t1a.microsecond

t1b = datetime.now()

print "This is the root of g(x) using the bisection method: ", bisection(g,1.0,2.0)

t2b = datetime.now()

print "This is the time for the bisection method for g(x) in microseconds: ", t2b.microsecond - t1b.microsecond

t1c = datetime.now()

print "This is the root of y(x) using the bisection method: ", bisection(y,0.0,5.0)

t2c = datetime.now()

print "This is the time for the bisection method for y(x) in microseconds: ", t2c.microsecond - t1c.microsecond

def newton_raphson(f,guess,tolerance=1.0e-5):
	if f(guess) == 0.0:
		return guess
	else:
		while (f(guess) > tolerance):
			fg = f(guess)
			
			if fg == 0.0:
				return guess
			
			elif fg != 0.0:
				h = 1.0e-5
				guess = guess - (f(guess) / ((f(guess+h)-f(guess-h))/ (2*h)))
		return guess

t1nrb = datetime.now()
		
print "This is the root for f(x) using the Newton Raphson method: ", newton_raphson(f,3.0,tolerance=1.0e-5)

t2nrb = datetime.now()

print "This is the time for the Newton Raphson method for f(x) in microseconds: ", t2nrb.microsecond - t1nrb.microsecond

t1nra = datetime.now()
		
print "This is the root for g(x) using the Newton Raphson method: ", newton_raphson(g,1.6,tolerance=1.0e-5)

t2nra = datetime.now()

print "This is the time for the Newton Raphson method for g(x) in microseconds: ", t2nra.microsecond - t1nra.microsecond

t1nrc = datetime.now()
		
print "This is the root for y(x) using the Newton Raphson method: ", newton_raphson(y,2.1,tolerance=1.0e-5)

t2nrc = datetime.now()

print "This is the time for the Newton Raphson method for y(x) in microseconds: ", t2nrc.microsecond - t1nrc.microsecond

print "As we can see, the Newton Raphson method is universally faster."

#end part one
#part two

def intensity(T):
	h = np.float64(6.6260755e-27)
	c = np.float64(2.99792458e10)
	k = np.float64(1.380658e-16)
	B = np.float64(1.25e-12)
	v = np.float64(344827586206.89655)
	
	return np.float64((((2.0*h*(v**3.0))/(c**2.0)) / (math.exp((h*v)/(k*T))-1.0))-B)

#now, I will define a high-precision newton raphson method

def newton_raphson_precision(f,guess,tolerance=1.0e-16):
		while (math.fabs(f(guess)) > tolerance):
			fg = f(guess)
			
			if math.fabs(fg) <= tolerance:
				return guess
			
			elif math.fabs(fg) >= tolerance:
				dx = 1.0
				guess = guess - (f(guess) / ((f(guess+dx)-f(guess-dx))/ (2*dx)))
		return guess
		
print "This is the temperature for corresponding to the given luminosity (in Kelvin): ", newton_raphson_precision(intensity, 30,tolerance=1.0e-16)




