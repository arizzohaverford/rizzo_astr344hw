import math

# I will begin with my random number generator for y and x

ax = 1664525.
cx = 1013904223
mx = 2.**32.

ay = 22695477
cy = 1
my = 2.**32.

def randomx(SEED):
	SEED = (ax*SEED+cx) % mx
	x = SEED / mx
	return x

def randomy(SEED):
	SEED = (ay*SEED+cy) % my
	y = SEED / my
	return y	
	
	
'''
now, we have a source of random numbers for our Monte Carlo simulation
so, we can go about calculating pi by placing a circle of radius r inside a square of 
side length 2r. Therefore, the ratio of the areas must be pi / 4 by basic algebra, and then
we know that if we pick random points within the square, then N * pi / 4 points should fall
inside the circle.
'''

n = 1000
inside = 0.

for i in range(0,n):
	x = randomx(i)
	y = randomy(i)
	if math.sqrt((x*x)+(y*y)) <= 1:
		inside += 1.
pi = (4*inside) / n

print "This is pi for n = 1000: ", pi

n = 10000
inside = 0.

for i in range(0,n):
	x = randomx(i)
	y = randomy(i)
	if math.sqrt((x*x)+(y*y)) <= 1:
		inside += 1.
pi = (4*inside) / n

print "This is pi for n = 10000: ", pi

n = 100000
inside = 0.

for i in range(0,n):
	x = randomx(i)
	y = randomy(i)
	if math.sqrt((x*x)+(y*y)) <= 1:
		inside += 1.
pi = (4*inside) / n

print "This is pi for n = 100000: ", pi

n = 1000000
inside = 0.

for i in range(0,n):
	x = randomx(i)
	y = randomy(i)
	if math.sqrt((x*x)+(y*y)) <= 1:
		inside += 1.
pi = (4*inside) / n

print "This is pi for n = 1000000: ", pi

print "Therefore, to get to pi = 3.141, it takes roughly 1000000 counts."


