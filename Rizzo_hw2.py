import numpy as np
y = np.float32(1.)
z = np.float32(3.)

# these are the single precision values that we will need

print "First, we will consider the recurrence relation x_n = (1/3)^n."

def xbar(n):
	if n == 0:
		return 1
	else:
		return ((y/z) ** n) + xbar(n-1)
		
# this is the single precision algorithm

print "These are the single precision values of x for n = 0 through n = 5:"
print (xbar(0))
print (xbar(1))
print (xbar(2))
print (xbar(3))
print (xbar(4))
print (xbar(5))

a = np.float64(1.)
b = np.float64(3.)

# these are the double precision values that we will need

def x(n):
	if n == 0:
		return 1
	else:
		return ((a/b) ** n) + x(n-1)
		
# this is the double precision algorithm

print "These are the double precision values of x for n = 0 through n = 5:"
print (x(0))
print (x(1))
print (x(2))
print (x(3))
print (x(4))
print (x(5))

		
absolute_error_5 = x(5) - xbar(5)
# this gives the absolute error for the case of n = 5

relative_error_5 = (x(5) - xbar(5)) / x(5)
# this gives the absolute error for the case of n = 5

absolute_error_20 = x(20) - xbar(20)
# this gives the absolute error for the case of n = 20

relative_error_20 = (x(20) - xbar(20)) / x(20)
# this gives the absolute error for the case of n = 20

print "The absolute error for n = 5 is:", absolute_error_5
print "The relative error for n = 5 is:", relative_error_5
print "The absolute error for n = 20 is:", absolute_error_20
print "The relative error for n = 20 is:", relative_error_20

print "Now, we will consider a new recurrence relation, x_n = 4^n"
# now we will consider a new function

c = np.float32(4.)

# this is the single precision value that we will need

def expbar(n):
	if n == 0:
		return 1
	else:
		return (c ** n) + expbar(n-1)
		
# this is the single precision algorithm

d = np.float64(4.)

def exp(n):
	if n == 0:
		return 1
	else:
		return (d ** n) + exp(n-1)
		
# this is the double precision algorithm

f = np.float32(expbar(20))

# I couldn't get the output of expbar to be single precision, so I used the variable 'f' to force it into single precision.

print "The single precision value of this function for n = 20 is:", f
print "The double precision value of this function for n = 20 is:", exp(20)

absolute_error_exp_20 = exp(20) - f
# this gives the absolute error for the case of n = 20

relative_error_exp_20 = (exp(20) - f) / exp(20)
# this gives the relative error for the case of n = 20

print "The absolute error for n = 20 is:", absolute_error_exp_20
print "The relative error for n = 20 is:", relative_error_exp_20

print "This calculation is stable because the relative error is an extremely small"
print "number. This basically means that in relation to the number as a whole, the"
print "error is smaller by many orders of magnitude. Therefore, relative error is"
print "a better indicator of accuracy and stability because while the absolute error"
print "may be large, this ultimately only matters in relation to the number itself."





