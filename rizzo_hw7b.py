import random
#first, I will begin with my random number generator for integers between 1 and 365
#however, due to the nature of this program, I will use random seeds 

a = 5
c = 1
m = 365

def randgen(SEED):
	SEED = (a*SEED+c) % m
	return SEED
	
#now that we have established our random birthdays, we will consider different class sizes
#we will leave the number of trials constant at 10000
#first, consider a class of 10 people

n = 10000
size = 10
samecount = 0

for i in range(n):
	list = []
	for j in range(size):
		bday = random.randrange(365)
		list.append(bday)
		
	same = False
	for k in list:
		if list.count(k) > 1:
			same = True
	
	if same == True:
		samecount += 1.
		
probability = samecount / 10000.

print "This is the probability for a class of 10 students: ", probability
print "Now we will try a class of 50 students: "

n = 10000
size = 50
samecount = 0

for i in range(n):
	list = []
	for j in range(size):
		bday = random.randrange(365)
		list.append(bday)
		
	same = False
	for k in list:
		if list.count(k) > 1:
			same = True
	
	if same == True:
		samecount += 1.
		
probability = samecount / 10000.

print "This is the probability for a class of 50 students: ", probability
print "Now we will try a class of 30 students: "

n = 10000
size = 30
samecount = 0

for i in range(n):
	list = []
	for j in range(size):
		bday = random.randrange(365)
		list.append(bday)
		
	same = False
	for k in list:
		if list.count(k) > 1:
			same = True
	
	if same == True:
		samecount += 1.
		
probability = samecount / 10000.

print "This is the probability for a class of 30 students: ", probability
print "Now, we will try a class of 20 students: "

n = 10000
size = 20
samecount = 0

for i in range(n):
	list = []
	for j in range(size):
		bday = random.randrange(365)
		list.append(bday)
		
	same = False
	for k in list:
		if list.count(k) > 1:
			same = True
	
	if same == True:
		samecount += 1.
		
probability = samecount / 10000.

print "This is the probability for a class of 20 students: ", probability
print "Now, we will try a class of 23 students: "

n = 10000
size = 23
samecount = 0

for i in range(n):
	list = []
	for j in range(size):
		bday = random.randrange(365)
		list.append(bday)
		
	same = False
	for k in list:
		if list.count(k) > 1:
			same = True
	
	if same == True:
		samecount += 1.
		
probability = samecount / 10000.

print "This is the probability for a class of 23 students: ", probability
print "Finally, we will try a class of 22 students to make sure that 23 is the minimum:"

n = 10000
size = 22
samecount = 0

for i in range(n):
	list = []
	for j in range(size):
		bday = random.randrange(365)
		list.append(bday)
		
	same = False
	for k in list:
		if list.count(k) > 1:
			same = True
	
	if same == True:
		samecount += 1.
		
probability = samecount / 10000.

print "This is the probability for a class of 22 students: ", probability

print "Since the probability of 22 students is less than 50%, we know that 23 must"
print "be the minimum number to reach 50%."


	

