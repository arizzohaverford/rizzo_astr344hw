import numpy as np
import matplotlib.pyplot as plt
import math

b = np.array([2.3, 3.4, 4.6, 5.7, 6.9, 8.0, 9.1, 10.3, 11.5, 12.6])
# this is the luminosity of each respective galaxy

n = np.array([8030, 2676, 773, 354, 200, 110, 65, 41, 27, 13, 6, 2])
# this is the number of galaxies with luminosities greater than a certain value

h_one = np.array([1.2, 1.2, 1.1, 1.2, 1.1, 1.2, 1.1, 1.1, 1.2, 1.2, 1.1, 1.2])
# these are the values of h1 that we will need for the Taylor series expansion for f prime

h_two = np.array([1.2, 1.1, 1.2, 1.1, 1.2, 1.1, 1.1, 1.2, 1.2, 1.1, 1.2, 1.2])
# these are the values of h2 that we will need for the Taylor series expansion for f prime

fprime = []
# I will build up this list to use for all of the dn/dl values of the model


fprime.append(((h_one[1]/(h_two[1]*(h_one[1]+h_two[1])))*(n[2]))-(((h_one[1]-h_two[1])/(h_two[1]*h_one[1]))*(n[1]))-((h_two[1])/(h_one[1]*(h_one[1]+h_two[1]))*(n[0])))
# this is fprime for the first value, using a Taylor series
# I will repeat this process for all twelve values of fprime, since I do not know how to wrap it up into one nice function.
# Also, I chose to omit the endpoints because this approximation does not work properly without a value of h from the left and right sides.

fprime.append(((h_one[2]/(h_two[2]*(h_one[2]+h_two[2])))*(n[3]))-(((h_one[2]-h_two[2])/(h_two[2]*h_one[2]))*(n[2]))-((h_two[2])/(h_one[2]*(h_one[2]+h_two[2]))*(n[1])))

fprime.append(((h_one[3]/(h_two[3]*(h_one[3]+h_two[3])))*(n[4]))-(((h_one[3]-h_two[3])/(h_two[3]*h_one[3]))*(n[3]))-((h_two[3])/(h_one[3]*(h_one[3]+h_two[3]))*(n[2])))

fprime.append(((h_one[4]/(h_two[4]*(h_one[4]+h_two[4])))*(n[5]))-(((h_one[4]-h_two[4])/(h_two[4]*h_one[4]))*(n[4]))-((h_two[4])/(h_one[4]*(h_one[4]+h_two[4]))*(n[3])))

fprime.append(((h_one[5]/(h_two[5]*(h_one[5]+h_two[5])))*(n[6]))-(((h_one[5]-h_two[5])/(h_two[5]*h_one[5]))*(n[5]))-((h_two[5])/(h_one[5]*(h_one[5]+h_two[5]))*(n[4])))

fprime.append(((h_one[6]/(h_two[6]*(h_one[6]+h_two[6])))*(n[7]))-(((h_one[6]-h_two[6])/(h_two[6]*h_one[6]))*(n[6]))-((h_two[6])/(h_one[6]*(h_one[6]+h_two[6]))*(n[5])))

fprime.append(((h_one[7]/(h_two[7]*(h_one[7]+h_two[7])))*(n[8]))-(((h_one[7]-h_two[7])/(h_two[7]*h_one[7]))*(n[7]))-((h_two[7])/(h_one[7]*(h_one[7]+h_two[7]))*(n[6])))

fprime.append(((h_one[8]/(h_two[8]*(h_one[8]+h_two[8])))*(n[9]))-(((h_one[8]-h_two[8])/(h_two[8]*h_one[8]))*(n[8]))-((h_two[8])/(h_one[8]*(h_one[8]+h_two[8]))*(n[7])))

fprime.append(((h_one[9]/(h_two[9]*(h_one[9]+h_two[9])))*(n[10]))-(((h_one[9]-h_two[9])/(h_two[9]*h_one[9]))*(n[9]))-((h_two[9])/(h_one[9]*(h_one[9]+h_two[9]))*(n[8])))

fprime.append(((h_one[10]/(h_two[10]*(h_one[10]+h_two[10])))*(n[11]))-(((h_one[10]-h_two[10])/(h_two[10]*h_one[10]))*(n[10]))-((h_two[10])/(h_one[10]*(h_one[10]+h_two[10]))*(n[9])))


absfprime = []

for x in fprime:
	 absfprime.append(abs(x))
	 
# I needed to take absolute values since all of the numbers came out negative
	 
differentialcounts = []
	 
for y in absfprime:
	differentialcounts.append(math.log10(y))
	
# I took the logarithm of the model dn/dl so that it would match the observed data
	
lum = []

for z in b:
	lum.append(math.log10(z))
	
# By the same logic, I took the logarithm of the model luminosity
	
data = np.loadtxt('ncounts_850.dat')

# This is the observed data

dndl = data[:,1]
luminosity = data[:,0]

x = range(-1,3)
y = range(-5,7)
fig = plt.figure()
ax1 = fig.add_subplot(111)

ax1.scatter(lum, differentialcounts, s=75, c='b', marker="s", label='model')
ax1.scatter(luminosity, dndl, s=20, c='r', marker="o", label='observed')
plt.legend(loc='upper left');
plt.show()

plt.savefig('Computational_Astro_HW3.jpg')

# As we can see, the model does a very good job of estimating the observed data

	


	

