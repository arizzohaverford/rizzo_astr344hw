import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import math 

trials1 = [range(20)]
times1 = [141,197,192,185,116,129,225,173,157,180,206,170,170,228,353,96,260,95,226,272]

average = np.average(times1)

print average

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(trials1, times1, s=50, c='r', marker="o", label = 'Time per Trial')
ax1.set_xlim([0,20])
ax1.set_ylim([0,500])
ax1.set_xlabel('Trial #')
ax1.set_ylabel('Time to Infect All People (seconds)')

plt.title("Total Infection Time")
plt.legend(loc='upper left');
plt.show()


people_infected1 = [1,2,3,4,5,7,8,9,10,11,12,17,22,27,28,31,33,34,36,37,38,40,41,43,44,48,49,51,52,53,54,56,57,58,59,62,63,66,70,72,73,74,75,76,77,78,79,80,81,82,83,84,85,87,88,89,90,91,92,93,94,95,96,97,98,99,100]
time = [1,5,6,7,8,10,12,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,30,31,33,35,36,37,39,40,42,43,44,45,46,47,49,50,53,55,56,58,59,61,63,64,65,72,74,76,80,83,84,86,87,91,94,96,97,100,103,130,179,180,227,355,356]


fig = plt.figure()
ax2 = fig.add_subplot(111)
ax2.scatter(time, people_infected1, s=50, c='b', marker="s", label = 'People Infected')
ax2.set_xlim([0,286])
ax2.set_ylim([0,100])
ax2.set_xlabel('Time')
ax2.set_ylabel('Number of People Infected')

plt.title("Number of People Infected Versus Time")
plt.legend(loc='lower right');
plt.show()

number_vaccinated = [0,10,20,30,40,50,60,70,80,90,95]
time_to_infect = [188,204,227,249,250,288, 273,292,303,361,367]

fig = plt.figure()
ax3 = fig.add_subplot(111)
ax3.scatter(number_vaccinated, time_to_infect, s=50, c='g', marker="s", label = 'Time to Infection')
ax3.set_xlim([0,100])
ax3.set_ylim([0,400])
ax3.set_xlabel('Number of People Vaccinated')
ax3.set_ylabel('Time to Infect All Others')

plt.title("Time to Infect Others Versus Number of People Vaccinated")
plt.legend(loc='upper left');
plt.show()

trials2 = [range(20)]
times2 = [290,359,215,315,176,201,343,208,413,296,228,247,309,157,194,245,297,200,203,430]

average1 = np.average(times2)

print average1

fig = plt.figure()
ax4 = fig.add_subplot(111)
ax4.scatter(trials2, times2, s=50, c='y', marker="o", label = 'Time per Trial')
ax4.set_xlim([0,20])
ax4.set_ylim([0,500])
ax4.set_xlabel('Trial #')
ax4.set_ylabel('Time to Infect All People (seconds)')

plt.title("Total Infection Time When People Avoid Each Other")
plt.legend(loc='upper left');
plt.show()


